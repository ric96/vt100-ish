import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
import subprocess
import time
import serial.tools.list_ports as ser_dev
import os

flag = 0

root = tk.Tk()
root.config(cursor='none')
root.geometry("800x480")

termin = [0, 1, 2, 3]
com_ports_name = [0]

global comPorts

button_frame = tk.Frame(root, borderwidth=10)
button_frame.pack(side=tk.TOP)

quit_button_frame = tk.Frame(root, borderwidth=10)
quit_button_frame.pack(side=tk.BOTTOM)

term_master = tk.Frame(root, width=505, height=320, borderwidth=10)
term_master.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

def show_term(term):
	if term.flag == 0:
		term.proc = subprocess.Popen('uxterm -sb -rightbar -geometry 80x24 -into %d &' % term.wid, shell=True)

	for i in termin:
		if i == term:
			i.term_frame.pack(fill=tk.BOTH, expand=1)
			i.options_frame.pack(side=tk.RIGHT)
			if term.flag == 0:
				time.sleep(1)
				f=open("curtty.txt", "r")
				term.pts_str = f.readline().strip()
				f.close()
				term.flag = 1
			print(term.pts_str+" help")

		elif i != term:
			i.term_frame.pack_forget()
			i.options_frame.pack_forget()

def connect(self):
	command = "./ttyecho -n "+self.pts_str+" picocom "+str(self.curr_port)+" -b "+self.curr_baud.get()+" -y "+self.curr_parity.get()+" -d "+self.curr_bits.get()+" -p "+self.curr_stop.get()+" &"
	print(command)
	os.system(command)


def refresh(self):
	comPorts = ser_dev.comports()
	i = 0
	for p in comPorts:
		if i == 0:
			com_ports_name[i] = p.device
		else:
			com_ports_name.append(p.device)
		print(com_ports_name[i])
		i = i + 1
	self.com_port_var = tk.StringVar()
	self.com_port_var.set(com_ports_name[0])
	self.serial_dev_drop = tk.OptionMenu(self.options_frame, self.com_port_var, *com_ports_name, command=self.get_selection).grid(row=1, column=1)	
	print(len(comPorts))

class tabs:
	def get_selection(self,value):
		print(value)
		self.curr_port = value

	def __init__(self, term_name):
		self.term_name = term_name
		self.flag = 0
		self.pts_str = " "
		self.com_port_var = tk.StringVar()
		self.com_port_var.set(com_ports_name[0])
		self.curr_port = tk.StringVar()
		self.curr_baud = tk.StringVar()
		self.curr_bits = tk.StringVar()
		self.curr_parity = tk.StringVar()
		self.curr_stop = tk.StringVar()
		self.term_frame = tk.Frame(term_master)
		self.wid = self.term_frame.winfo_id()
		self.tab_button = tk.Button(button_frame, text=self.term_name, command=lambda : show_term(self), height=3)
		self.tab_button.pack(side=tk.LEFT)
		self.options_frame = tk.Frame(root, borderwidth=10)
		self.serial_dev = tk.Label(self.options_frame, text="Serial Port: ").grid(row=1, column=0)
		refresh(self)
		self.baud = tk.Label(self.options_frame, text="Baud: ").grid(row=2, column=0)
		self.baud_entry = tk.Entry(self.options_frame, width=10, textvariable=self.curr_baud).grid(row=2, column=1)
		self.bits = tk.Label(self.options_frame, text="Bits: ").grid(row=3, column=0)
		self.bits_entry = tk.Entry(self.options_frame, width=10, textvariable=self.curr_bits).grid(row=3, column=1)
		self.parity = tk.Label(self.options_frame, text="Parity: ").grid(row=4, column=0)
		self.parity_entry = tk.Entry(self.options_frame, width=10, textvariable=self.curr_parity).grid(row=4, column=1)
		self.stop = tk.Label(self.options_frame, text="Stop Bits: ").grid(row=5, column=0)
		self.stop_entry = tk.Entry(self.options_frame, width=10, textvariable=self.curr_stop).grid(row=5, column=1)
		self.connect = tk.Button(self.options_frame, text="Connect", command=lambda : connect(self), height=1).grid(row=6, column=0)
		self.refresh = tk.Button(self.options_frame, text="Refresh", command=lambda : refresh(self), height=1).grid(row=6, column=1)

def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        proc1 = subprocess.Popen('pkill xterm &', shell=True)
        root.destroy()

termin[0] = tabs("Term 1")
termin[1] = tabs("Term 2")
termin[2] = tabs("Term 3")
termin[3] = tabs("Term 4")
show_term(termin[0])

root.quit_button = tk.Button(quit_button_frame, text="quit", command = on_closing, height=1)
root.quit_button.pack(side=tk.LEFT)

root.protocol("WM_DELETE_WINDOW", on_closing)

root.update()

print (root.winfo_width())
print (root.winfo_height())
print (root.winfo_geometry())

root.mainloop()

exit()
